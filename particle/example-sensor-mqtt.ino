/* Libraries */
// The library for the accelerometer
#include <LIS3DH.h>
// The library for the MQTT connection
#include "MQTT.h"

//ATTENTION Change the foloowing 5 values
//=====================================================================================
//A unique id for your particle (usually the device Id) -- find in IOSENSE
const String DEVICE_ID = "REPLACE_ME_BY_YOUR_DEVICE_ID"; // for example: particle-2c0046001347343438323536";
//Id of you Thing -- find in IOSENSE
const String THING_ID = "REPLACE_ME_BY_YOUR_THING_ID" ;//for example wolf-test2-35e8";
// JSON Web Token provided when you created your Thing on ioSense -- find in IOSENSE
const String JWT = "REPLACE_ME_BY_A_VERY_LONG_TOKEN ";// for example "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1MjU3MDIwNDMsImV4cCI6MTg0MTI3ODA0MywiYXVkIjoiaHR0cDovL2lvc2Vuc2UudHVkZWxmdC5ubDo4MCJ9.Eo8YqP5H2a1QMeFgZ096UnPDSPXOjycM5CW8_htptczLvffSnxBNQs7dd_rvFIf2CLfJQmzjzhzZaDzkFO3yeKgJmNxOWoLaE9aQqRYTaAlTclE8pR847movdqj5MT6Bd0ugQo7m3Sqac2zVUuDF7RMZcYQ1jBXY1RIFTcHjeFgpSgjh45hU2aYK3kj1V3ngwf6eDS24ILc2wWOhQTP8kA6iyOfGURwd1wdF-RwXTg4b65sPUcaXVEeiYPsm_P100gk5Z39RoCBRIlNG3p3u5ZsC_7A0UtUxHxw21Z9Wjcz_acLOltLLf7jRYDccJDqT5JSJ5XqrH9NWY2n05eQ77_FaUs7zsn8s1We7YgECLDDPNd4CgPeHgOidTLroYV028CXSEVJL4GPPwWyiScHIF_w1rOvjKfD2gkbhI8Nn1kls2HPhtBosQSmlLpQ59szbjWc6-zDL0Ugq9J2qOI4lRV69wUHfesS-uJxOa3FGG-as-vyWa7ofEFJoA6fXKv_uTntLPmoG0zoohy05nvK5YDW6j9KLHYNRrUJZiupfMp3Ifyt47VRHnP8nTbPgF63Gsj45bEWFyy3TvwPBkusx18yvIN519fiLtLoEqGs_x3fyW_7Epy6_nPblIY9p-TD1qrx9I2KG4vt5HXSAW1WjSdxqetKmk7hcszy6dsHCFXY";// Thetopic to publish data on MQTT
//Accelerometer name -- find in IOSENSE
const String ACC_MEASURE = "REPLACE_ME_BY_YOUR_ACCERLOMETER_ID";//for example: accelerlometer
//Sound sensor name -- find in IOSENSE
const String SOUND_MEASURE = "REPLACE_ME_BY_YOUR_SOUND_ID";//for example: sound
//position sensor name -- find in IOSENSE
const String POSITION_MEASURE  = "REPLACE_ME_BY_YOUR_POSITION_ID"; //for example: position
//is On sensor name -- find in IOSENSE
const String IS_ON_MEASURE  = "REPLACE_ME_BY_YOUR_IS_ON_ID"; //for example: is-on

/**
 * Constants
 */

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
const String MQTT_TOPIC = "/things/" + THING_ID + "/properties/";
// How often we take a sample from the accelerometer (in milliseconds)
const unsigned long SAMPLE_PERIOD = 100;
// How often we push the sample to ioSense (in milliseconds)
const unsigned long PUBLISH_PERIOD = 2000;

/**
 * Variables
 */

// Declare and initialise the last publish and sample time at 0
unsigned long lastSample = 0;
unsigned long lastPublished = 0;

// Declare the accelerometer
LIS3DHSPI accel(SPI, A2, WKP);
// Declare the function called when receiving and position interuption
void positionInterruptHandler();
// Flag to informed that the device is detected in a new position
volatile bool positionInterrupt = false;
// Keep track of the last detected device postion
uint8_t lastPosition = 0;

// Keep track of the device start time
unsigned long startTimeUNIX;
// Store accelerometer values in this variable before publishing them on MQTT
String accValues = "";
// Store microphone values in this variable before publishing them on MQTT
String micValues = "";

// Declare the function
void sense(unsigned long millisSinceStart);
void senseAccelerometer(String millisSinceStart);
void senseMicrophone(String millisSinceStart);
void publishToIOSense(String millisSinceStart);

// Declare the function called when receiving a new message from MQTT
void onMQTTMessage(char* topic, byte* payload, unsigned int length);
// Implementation of the function called when receiving a new message from MQTT
void onMQTTMessage(char* topic, byte* payload, unsigned int length) {}

/**
 * if you want to use IP address,
 * byte server[] = { XXX,XXX,XXX,XXX };
 * MQTT client(server, 1883, callback);
 * if you want to use domain name,
 * MQTT client("www.sample.com", 1883, keepAlive, callback, maxBufferSize);
 **/
MQTT client("iosense.tudelft.nl", 1883, MQTT_DEFAULT_KEEPALIVE, onMQTTMessage, 1024);


/**
 * Convert NTP time to UNIX time
 */
time_t tmConvert_t(int YYYY, byte MM, byte DD, byte hh, byte mm, byte ss) {
  struct tm t;
  t.tm_year = YYYY-1900;
  t.tm_mon = MM - 1;
  t.tm_mday = DD;
  t.tm_hour = hh;
  t.tm_min = mm;
  t.tm_sec = ss;
  t.tm_isdst = 0;
  time_t t_of_day = mktime(&t);
  return t_of_day;
}

/** Having declared these variables, let's move on to the setup function.
 * The setup function is a standard part of any microcontroller program.
 * It runs only once when the device boots up or is reset.
 */
void setup() {
    
    Particle.syncTime();
    startTimeUNIX = tmConvert_t(Time.year(), Time.month(), Time.day(), Time.hour(), Time.minute(), Time.second());

    // Connect to the ioSense MQTT server
    // Use the device id as client id, the thing id as user with 'things:' prefix and the JSON Web Token as password
    client.connect(DEVICE_ID, "things:" + THING_ID, JWT);

    // Setup interuption
	attachInterrupt(WKP, positionInterruptHandler, RISING);
	delay(5000);

	// Initialize accelerometer
	LIS3DHConfig config;
	config.setPositionInterrupt(16);
	bool setupSuccess = accel.setup(config);
    
    // publish/subscribe
    if (client.isConnected()) {
        client.publish(MQTT_TOPIC + IS_ON_MEASURE, "{\"values\":[[" + String(startTimeUNIX) + "000,1]]}");
        // client.subscribe("/things/" + thingId + "/properties/#");
    }
    
    // Mic AUD connected to Analog pin 0
    pinMode(A0, INPUT);
    // Flash on-board LED
    pinMode(D7, OUTPUT);
    
}


/**
 * Next we have the loop function, the other essential
 * part of a microcontroller program. This routine gets
 * repeated over and over, as quickly as possible and
 * as many times as possible, after the setup function
 * is called.
 * 
 * Note: Code that blocks for too long (like more than
 * 5 seconds), can make weird things happen (like dropping
 * the network connection).  The built-in delay function
 * shown below safely interleaves required background
 * activity, so arbitrarily long delays can safely be
 * done if you need them.
 */
void loop() {
    
    // if we are connected to MQTT, maintain the connection
    if (client.isConnected()) {
        client.loop();
    }

	unsigned long millisSinceStart = millis();
    
    // Get data from sensors
    sense(millisSinceStart);
    // Publish data to ioSense
    publishToIOSense(millisSinceStart);
    
    // Catch position from accelerometer
	if (positionInterrupt) {
		positionInterrupt = false;
		// Test the position interrupt support. Normal result is 5.
		// 5: normal position, with the accerometer facing up
		// 4: upside down
		// 1 - 3: other orientations
		uint8_t position = accel.readPositionInterrupt();
	   if (client.isConnected()) {
    		if (position != 0 && position != lastPosition) {
	           // Divide millisSinceStart by 1000 to get seconds, and add up to the start time
                unsigned long timestamp = startTimeUNIX + millisSinceStart/1000;
                // Use % to extract the modulo 1000, rest of division by 1000, i.e. the number of milliseconds
                unsigned long millis = millisSinceStart % 1000;
                // Concat both seconds and milliseconds together
                String unixTime = String(timestamp) + String(millis);
    		   client.publish(MQTT_TOPIC + POSITION_MEASURE, "{\"values\":[[" + unixTime + "," + String(position) + "]]}");
    		   // Keep track of the last position
    		   lastPosition = position;
    		}   
	   }
	}
    
}

/**
 * Sense data
 */
void sense(unsigned long millisSinceStart) {
    if (millisSinceStart - lastSample >= SAMPLE_PERIOD) {
	   // Update the lastSample time to the current time
	   lastSample = millisSinceStart;
	   
	   // Divide millisSinceStart by 1000 to get seconds, and add up to the start time
        unsigned long timestamp = startTimeUNIX + millisSinceStart/1000;
        // Use % to extract the modulo 1000, rest of division by 1000, i.e. the number of milliseconds
        unsigned long millis = millisSinceStart % 1000;
        // Concat both seconds and milliseconds together
        String unixTime = String(timestamp) + String(millis);
        
        // Sense data from microphone
        senseMicrophone(unixTime);
        // Sense data from accelerometer
        senseAccelerometer(unixTime);
    }
}

/**
 * Collect new accelerometer data sample
 */
void senseAccelerometer(String unixTime) {
    LIS3DHSample sample;
    if (accel.getSample(sample)) {
        if (accValues.length() != 0) {
            accValues.concat(",");
        }
        accValues.concat("[" + unixTime + "," + String(sample.x) + "," + String(sample.y) + "," + String(sample.z) + "]");
    }
}

/**
 * Collect new microphone data sample
 */
void senseMicrophone(String unixTime) {
    // Read the value from the microphone
    int mic_reading = analogRead(0);
    // If the string containing microphone values is not empty,
    // we need to add a comma to seperate the next value
    if (micValues.length() != 0) {
        micValues.concat(",");
    }
    // Add the value as an array with only one value
    micValues.concat("[" + unixTime + "," + mic_reading + "]");
}

/**
 * Publish collected data to ioSense
 */
void publishToIOSense(unsigned long millisSinceStart) {
    // If we did not publish the data since at least PUBLISH_PERIOD, and we are connected to MQTT
    if (millisSinceStart - lastPublished >= PUBLISH_PERIOD && client.isConnected()) {
        // Then we update 
	   lastPublished = millisSinceStart;
	   client.publish(MQTT_TOPIC + ACC_MEASURE, "{\"values\":[" + accValues + "]}");
	   accValues = "";
	   client.publish(MQTT_TOPIC + SOUND_MEASURE, "{\"values\":[" + micValues + "]}");
	   micValues = "";
	}
}


/**
 * Called when the accelerometer detect a change of position
 */
void positionInterruptHandler() {
    // Set the positionInterupt flag to pick it up during the next loop
	positionInterrupt = true;
}

