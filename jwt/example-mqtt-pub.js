'use strict';

const IOSENSE_MQTT_URI = 'mqtt://iosense.tudelft.nl:1883';
const THING_ID = 'YOUR_THING_ID';
const PROPERTY_ID = 'YOUR_PROPERTY_ID';

const settings = {
  keepalive: 1000,
  protocolId: 'MQIsdp',
  protocolVersion: 3,
  clientId: 'YOUR-CLIENT-ID',
  username: 'YOUR-USERNAME',
  password: 'YOUR-JWT-TOKEN'
};

// Import JavaScript MQTT client library
const mqttLib = require('mqtt');
// Use the MQTT library to establish MQTT connection
const mqtt = mqttLib.connect(IOSENSE_MQTT_URI, settings);
// Define what to do when we 'connect',
// i.e. when the connection is established.
mqtt.on('connect', onMQTTConnect);
// Define what to do when we receive a message
mqtt.on('message', onMQTTMessage);

/**
 * Define what to do when we 'connect'.
 */
function onMQTTConnect() {
  console.log('Publisher connected: ' + mqtt.connected);
  mqtt.publish('/things/' + THING_ID + '/properties/' + PROPERTY_ID,
    JSON.stringify({
      values: [
        [new Date().getTime(),
          Math.random(),
          Math.random(),
          Math.random()]
      ]}));
}

/**
 * Called when our web server receive a message from the MQTT broker.
 **/
function onMQTTMessage(topic, message) {
  console.log('received: ' + message);
}