'use strict';

const IOSENSE_URI = 'https://iosense.tudelft.nl';
const THING_ID = 'YOUR_THING_ID';
const PROPERTY_ID = 'YOUR_PROPERTY_ID';

const jwt = require('jsonwebtoken');

const request = require('request-promise');

function sendData() {
  const privateKey = 'YOUR-PRIVATE-KEY';

  const token = {
    'iat': parseInt(Date.now() / 1000),
    'exp': parseInt(Date.now() / 1000) + 20 * 60, // 20 minutes
    'aud': IOSENSE_URI
  };

  const algorithm = 'RS256';
  const jwtToken = jwt.sign(token, privateKey, {algorithm: algorithm});

  const options = {
    method: 'PUT',
    uri: IOSENSE_URI + '/things/' + THING_ID + '/properties/' + PROPERTY_ID,
    body: {
      values: [
        [new Date().getTime(),
          Math.random(),
          Math.random(),
          Math.random()]
      ]
    },
    headers: {
      'User-Agent': 'Request-Promise',
      'Authorization': 'Bearer ' + jwtToken
    },
    json: true
  };

  return request(options)
    .then((result) => {
      console.error(result);
    }).catch((error) => {
      console.error(error);
    });
}

sendData();