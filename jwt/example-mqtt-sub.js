'use strict';

const IOSENSE_MQTT_URI = 'mqtt://iosense.tudelft.nl:1883';

const settings = {
  keepalive: 1000,
  protocolId: 'MQIsdp',
  protocolVersion: 3,
  clientId: 'YOUR-CLIENT-ID',
  username: 'YOUR-USERNAME',
  password: 'YOUR-JWT-TOKEN'
};

/**
 * Define what to do when we 'connect'.
 */
const onMQTTConnect = () => {
  console.log('Subscriber connected: ' + mqtt.connected);
  mqtt.subscribe('/things/wheelchair-41/properties/#');
};

/**
 * Called when our web server receive a message from the MQTT broker.
 **/
const onMQTTMessage = (topic, message) => {
  console.log('received: ' + message);
};

// Import JavaScript MQTT client library
const mqttLib = require('mqtt');
// Use the MQTT library to establish MQTT connection
const mqtt = mqttLib.connect(IOSENSE_MQTT_URI, settings);
// Define what to do when we 'connect',
// i.e. when the connection is established.
mqtt.on('connect', onMQTTConnect);
// Define what to do when we receive a message
mqtt.on('message', onMQTTMessage);